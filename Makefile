# Makefile

ASM = nasm -f elf64 -I src -o
LD = ld -o
RM = rm -f
PYTHON = python3

.PHONY: all clean

all: out/main


out/main.o: src/main.asm out/lib.o out/dict.o
	$(ASM) $@ $<

out/dict.o: src/dict.asm src/dict.inc
	$(ASM) $@ $<

out/lib.o: src/lib.asm src/lib.inc
	$(ASM) $@ $<



out/main: out/lib.o out/main.o out/dict.o
	$(LD) $@ $^
	$(RM) out/*.o

clean:
	$(RM) out/*

test:
	$(PYTHON) test.py



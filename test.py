import subprocess


test_input_list = [
    "",
    "qwerty",
    "first_word",
    "second word",
    "third_word",
    "a" * 255,
    "c" * 256,
    "b" * 300
]

test_output_list = [
    "No such key",
    "No such key",
    "first word explanation",
    "second word explanation",
    "third word explanation",
    "No such key",
    "Too long key",
    "Too long key"
]


def print_test_result(test_number, success, test_input, output, expected_ouptup):
    if (success):
        print(f"TEST {test_number}: OK")
    else:
        print(f"TEST {test_number}: FAIL")
        print(f"Input: {test_input}")
        print(f"Output: {output}")
        print(f"Expected output: {expected_ouptup}")


APP_PATH = "out/main"

def run_test(test_number, test_input, expected_ouptup):
    process = subprocess.Popen([APP_PATH], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
    stdout = process.communicate(test_input.encode())[0]
    output = stdout.decode().strip()
    success = output == expected_ouptup
    print_test_result(test_number, success, test_input, output, expected_ouptup)
    

test_amount = len(test_input_list)

for i in range(test_amount):
    test_number = i + 1
    test_input = test_input_list[i]
    test_output = test_output_list[i]
    run_test(test_number, test_input, test_output)

global exit
global string_length
global print_string
global print_newline
global print_char
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global read_line


%define STDOUT 1
%define WRITE_SYSCALL 1
%define EXIT_SYSCALL 60

%define SPACE 0x20
%define TAB 0x9
%define LINE_BREAK 0xA


section .text
 

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, EXIT_SYSCALL
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax    ; длина строки
    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax     ; длина строки
        jmp .loop
    .end:
        ret    

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi               ; указатель на нуль-терминированную строку
    call string_length
    mov rdx, rax           ; длина строки
    pop rsi                ; указатель на нуль-терминированную строку
    mov rax, WRITE_SYSCALL
    mov rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, LINE_BREAK    ; символ перевода строки
    ; далее начинает выполняться print_char

; Принимает код символа и выводит его в stdout
print_char:
    push rdi            ; код символа
    mov rsi, rsp        ; адрес кода символа
    mov rax, WRITE_SYSCALL          ; номер системного вызова 'write'
    mov rdi, STDOUT     ; дескриптор стандартного потока вывода
    mov rdx, 1          ; количество байт для вывода
    syscall
    pop rdi
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
print_uint:
    enter 0, 0
    push 0              ; нуль-терминатор
    mov rax, rdi        ; делимое: младшие 8 байт
    mov rsi, 10         ; делитель
    .div_loop:
        xor rdx, rdx    ; делимое: старшие 8 байт
        div rsi
        add dl, '0'     ; ASCII код остатка от деления
        dec rsp
        mov byte[rsp], dl
        test rax, rax
        je .print
        jmp .div_loop
    .print:
        mov rdi, rsp    ; адрес начала строки
        leave
        jmp print_string
    

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi
    jge .not_negative
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    .not_negative:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    .loop:
        mov dl, byte[rdi]   ; символ первой строки
        mov cl, byte[rsi]   ; символ второй строки
        cmp dl, cl
        jne .not_equals
        test dl, dl         ; проверка на нуль-терминатор
        je .equals
        inc rdi             ; адрес следующего символа первой строки 
        inc rsi             ; адрес следующего символа второй строки
        jmp .loop
    .equals:
        mov rax, 1
        ret
    .not_equals:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    xor rax, rax        ; номер системного вызова 'read'
    xor rdi, rdi        ; дескриптор стандартного потока ввода
    mov rdx, 1          ; количество байт для ввода
    mov rsi, rsp
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале.
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
read_word:
    push rbx            ; callee-saved регистр
    push r12            ; callee-saved регистр
    push r13            ; callee-saved регистр
    push r14            ; callee-saved регистр
    push rdi            ; адрес начала буфера
    xor rbx, rbx        ; флаг: пропущены ли пробельные символы в начале (0 - нет, 1 - да)
    xor r12, r12        ; длина слова
    mov r13, rdi        ; адрес ячейки буфера
    mov r14, rsi        ; статочный размер буфера
    .loop:
        call read_char  ; прочитать следующий символ в rax

        cmp rax, SPACE   ; проверка на пробел
        je .end_word_check
        cmp rax, TAB    ; проверка на табуляцию
        je .end_word_check
        cmp rax, LINE_BREAK    ; проверка на перевод строки
        je .end_word_check

        or bl, 1        ; установка флага: пробельные символы в начале пропущены

        test r14, r14   ; проверка оставшейся длины буфера
        je .fail

        .write:
        mov byte[r13], al

        test rax, rax   ; проверка на нуль терминатор
        je .end

        inc r12         ; длина слова
        inc r13         ; адрес следующей ячейки буфера
        dec r14         ; остаточный размер буфера
        jmp .loop

    .end_word_check:    ; обработка пробельного символа - проверка на конец слова
        test rbx, rbx   ; проверка флага: пропущены ли пробельные символы в начале
        je .loop        ; если не были пропущены - пропускаем
        xor rax, rax    ; если были пропущены - это конец слова, нуль-терминатор в rax
        jmp .write

    .fail:
        mov qword[rsp], 0

    .end:
        pop rax         ; адрес буфера или 0 (в случае неуспеха)
        mov rdx, r12    ; длина слова 
        pop r14         ; callee-saved регистр
        pop r13         ; callee-saved регистр
        pop r12         ; callee-saved регистр
        pop rbx         ; callee-saved регистр
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rsi, rsi        ; символ цифры числа
    xor rax, rax        ; число в двоичном представлении
    mov r11, rdi        ; адрес символа
    mov r10, 10         ; множитель
    .loop:
        mov sil, [r11]  ; символ цифры числа
        cmp sil, '0'
        jb .end
        cmp sil, '9'
        ja .end
        sub sil, '0'    ; цифра числа в двоичном представлении
        mul r10
        add rax, rsi    ; число в двоичном представлении
        inc r11         ; адрес символа
        jmp .loop
    .end:
        sub r11, rdi    ; длина числа в символах
        mov rdx, r11
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    mov sil, [rdi]      ; первый символ числа в строковом представлении
    cmp sil, '+'
    je .plus
    cmp sil, '-'
    je .minus
    jmp parse_uint
    .plus:
        inc rdi         ; адрес первой цифры положительного числа
        call parse_uint
        jmp .check
    .minus:
        inc rdi         ; адрес первой цифры отрицательного числа
        call parse_uint
        neg rax         ; отрицательное число в двоичном представлении
    .check:
        test rdx, rdx   ; удалось ли прочитать беззнаковое число (проверка длины числа)
        je .end         
        inc rdx         ; учесть символ знака ('-' или '+') в длине числа в символах
    .end:
        ret
        
; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        test rdx, rdx       ; проверка оставшейся длины буфера
        jna .fail
        mov cl, byte[rdi]   ; символ строки
        mov byte[rsi], cl   ; копирование символа строки
        test cl, cl         ; проверка на нуль-терминатор
        je .end
        dec rdx             ; длина буфера
        inc rdi             ; адрес следующего символа строки
        inc rsi             ; адрес следующей ячейки буфера
        inc rax             ; длина строки
        jmp .loop
    .fail:
        xor rax, rax        ; возвращаемое значение при неуспехе
    .end:
        ret


; NEW!
; Принимает: адрес начала буфера, размер буфера.
; Читает в буфер слово из stdin до перевода строки 0xA, дописывает нуль-терминатор.
; Останавливается и возвращает 0 если слово слишком большое для буфера.
; При успехе возвращает адрес буфера в rax, длину стоки в rdx.
; При неудаче возвращает 0 в rax.

read_line:
    push r12            ; callee-saved регистр
    push r13            ; callee-saved регистр
    push r14            ; callee-saved регистр
    push rdi            ; адрес начала буфера
    xor r12, r12        ; длина строки
    mov r13, rdi        ; адрес ячейки буфера
    mov r14, rsi        ; статочный размер буфера
    .loop:
        call read_char  ; прочитать следующий символ в rax

        cmp rax, LINE_BREAK    ; проверка на перевод строки
        je .line_break


        test r14, r14   ; проверка оставшейся длины буфера
        je .fail

        .write:
        mov byte[r13], al

        test rax, rax   ; проверка на нуль терминатор
        je .end

        inc r12         ; длина строки
        inc r13         ; адрес следующей ячейки буфера
        dec r14         ; остаточный размер буфера
        jmp .loop

    .line_break:        ; обработка перевода строки
        xor rax, rax    ; конец строки, нуль-терминатор в rax
        jmp .write

    .fail:
        mov qword[rsp], 0

    .end:
        pop rax         ; адрес буфера или 0 (в случае неуспеха)
        mov rdx, r12    ; длина строки 
        pop r14         ; callee-saved регистр
        pop r13         ; callee-saved регистр
        pop r12         ; callee-saved регистр
        ret
 
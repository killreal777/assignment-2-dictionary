%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define dictionary first_word

section .bss
buffer: resb 256            ; Выделение 256 байт под буфер в секции bss

section .rodata
not_found_message: db "No such key", 0
too_long_message: db "Too long key", 0

section .text

global _start
_start:
	mov rdi, buffer         ; Перемещение адреса буфера в rdi (первый аргумент для read_line)
	mov rsi, 256            ; Установка размера буфера в rsi (второй аргумент для read_line)
	call read_line

    test rax, rax           ; Проверка чтения ключа
	je .too_long            ; Если ключ слишком длинный, переход к метке .too_long

	mov rdi, buffer         ; Перемещение адреса буфера в rdi (первый аргумент для find_word)
	mov rsi, dictionary     ; Перемещение адреса словаря в rsi (второй аргумент для find_word)
	call find_word

	test rax, rax           ; Проверка результата поиска
	je .not_found           ; Если слово не найдено, переход к метке .not_found

	mov rdi, rax            ; Перемещение адреса найденного слова в rdi (первый аргумент для string_length)
	add rdi, 8              ; Смещение на 8 байт
	call string_length

	add rdi, rax            ; Прибавление длины строки к адресу слова в rdi (получение конечного адреса строки)
	inc rdi                 ; Инкремент адреса (переход к символу после строки)
	call print_string

	call print_newline

	xor rdi, rdi
	call exit

    .too_long:
        mov rdi, too_long_message
        call print_string
        call print_newline
        jmp .err_exit

	.not_found:
		mov rdi, not_found_message
		call print_string
        call print_newline

    .err_exit:
		mov rdi, 1
		call exit

%define FIRST 0

%macro colon 2
    %ifid %2
        %ifstr %1
            %2: 
                dq FIRST
                db %1, 0
            %define FIRST %2
        %else 
            %error "Key must be an id"
        %endif
    %else
        %error "Key must be a str"
    %endif
%endmacro

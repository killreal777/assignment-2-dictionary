%include "lib.inc"

global find_word

section .text

find_word:
    push r14               ; Сохранение значения регистра r14 на стеке (callee-saved)
    push r15               ; Сохранение значения регистра r15 на стеке (callee-saved)

    .find_loop:
        test rsi, rsi      ; Проверка на конец списка
        jz .not_found      ; Если rsi равен нулю, переход к метке .not_found

		mov r14, rdi       ; Сохранение значения rdi
		mov r15, rsi       ; Сохранение значения rsi

        add rsi, 8         ; Переход к значению элемента
        call string_equals ; Сравнение строк

		mov rdi, r14       ; Восстановление значения rdi
		mov rsi, r15       ; Восстановление значения rsi

        test rax, rax      ; Проверка результата сравнения (равны ли строки)
        jnz .found         ; Если строки равны, переход к метке .found

        mov rsi, [rsi]     ; Переход к следующей записи в списке
        jmp .find_loop

    .found:
        mov rax, rsi       ; Загрузка адреса найденного слова в rax (возвращаемое значение)
		jmp .end

    .not_found:
        xor rax, rax       ; Установка rax в ноль (возвращаемое значение)
        
	.end:
		pop r15            ; Восстановление значения регистра r15 (callee-saved)
		pop r14            ; Восстановление значения регистра r14 (callee-saved)
        ret
